﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TankCoverEditor : EditorWindow
{
    TankScript m_Tank;

    public void OnEnable()
    {
        m_Tank = GameObject.FindObjectOfType<TankScript>();
    }

    private void OnGUI()
    {
        if(m_Tank == null)
        return;

        OnGUIFunc(m_Tank);
    }

    private void OnGUIFunc(TankScript tank)
    {
        var oldColor = GUI.backgroundColor;
        var background = new Texture2D(1, 1);
        var color = new Color32(254, 222, 62, 255);
        background.SetPixel(0, 0, color);
        // background.SetPixel(0, 0, new Color(0.5f, 0f, 0f, 1));
        background.Apply();
        
        var state = new GUIStyleState();
        state.background = background;

        var rowStyle = new GUIStyle();
        rowStyle.margin = new RectOffset(0, 0, 6, 6);
        rowStyle.normal = state;

        using(var rowScope = new GUILayout.HorizontalScope(rowStyle))
        {
            var labelStyle = new GUIStyle(GUI.skin.label);
            labelStyle.normal.textColor = new Color(1, 1, 1, 1);
            labelStyle.fontSize = 25;
            labelStyle.fontStyle = FontStyle.Bold;
            labelStyle.fixedHeight = labelStyle.CalcHeight(GUIContent.none, 1);

            EditorGUILayout.LabelField(
                "AndyTankCover",
                labelStyle,
                GUILayout.Width(300),
                GUILayout.Height(labelStyle.fixedHeight)
            );

            var fieldStyle = new GUIStyle(GUI.skin.textField);
            fieldStyle.fontSize = 20;
            fieldStyle.fontStyle = FontStyle.Normal;
            fieldStyle.fixedHeight = fieldStyle.CalcHeight(GUIContent.none, 1);

            var rect = EditorGUILayout.GetControlRect(GUILayout.MaxWidth(200));
            rect.height = fieldStyle.fixedHeight;

            if(rect.Contains(Event.current.mousePosition))
                GUI.backgroundColor = new Color32(254, 202, 42, 255);

            tank.tankName = EditorGUI.TextField(rect, tank.tankName, fieldStyle);

            tank.tankSize =
                EditorGUILayout.IntField(
                    tank.tankSize,
                    fieldStyle,
                    GUILayout.Width(94),
                    GUILayout.Height(fieldStyle.fixedHeight)
                );

        }
    }
    [MenuItem("Andy/TankCoverEditor")]
    public static void ShowWindow()
    {
        var window = GetWindow<TankCoverEditor>();
        window.minSize = new Vector2(350, 200);
        window.titleContent = new GUIContent("TankCoverEditor");
    }
}
